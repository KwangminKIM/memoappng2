var path = require( 'path' );
var webpack = require( 'webpack' );

function root(args) {
  args = Array.prototype.slice.call(arguments, 0);
  return path.join.apply(path, [__dirname].concat(args));
}

module.exports = {
    target : 'web',
    resolve: {
      extensions: ['.ts', '.js', '.json'],
      modules: [ root('node_modules') ]
    },
    entry: './src/main.ts',
    output: {
      filename: 'bundle.js',
      path: 'dist',
      libraryTarget: 'var'
    },
    module: {
      rules: [
        { test: /\.ts$/,   use: [ 'awesome-typescript-loader',
                                  'angular2-template-loader',
                                  'angular-router-loader' ] },
        { test: /\.html$/, use: 'raw-loader' },
        { test: /\.css$/,  use: 'raw-loader' },
        { test: /\.json$/, use: 'json-loader' }
      ],
    }
  };
