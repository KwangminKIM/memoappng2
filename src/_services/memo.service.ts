import { Injectable } from '@angular/core';

import { Memo } from '../_classes/memo';

@Injectable()
export class MemoService{


// getMemoId : function($stateParams) {
//         console.log('getMemoId()');
//         return $q(function(resolve, reject){
//             try {
//                 var memoId = $stateParams.memoId, content = localStorage.getItem(memoId);
//                 resolve(contents);
//             } catch (e) {
//                 reject(e);
//             }
//         });

//     },
//     writeMemo : function(content) {
//         console.log('writeMemo()');
//         return $q(function(resolve, reject){
//             try {
//                 var memoId = 0;
//                 for(i in localStorage) { memoId = i; }
//                 memoId = parseInt(memoId) + 1;
//                 localStorage.setItem(memoId, content)
//                 resolve(true);
//             } catch (e) {
//                 console.log(e);
//                 reject(false);
//             }
//         });
//     },
//     modifyMemo : function(memoId, content) {
//         console.log('modifyMemo()');
//         return $q(function(resolve, reject){
//             try {
//                 localStorage.setItem(memoId, content)
//                 resolve(true);
//             } catch (e) {
//                 console.log(e);
//                 reject(false);
//             }
//         });
//     },
//     deleteMemo : function(memoId) {
//         console.log('deleteMemo()');
//         return $q(function(resolve, reject){
//             try {
//                 localStorage.removeItem(memoId);
//                 resolve(true);
//             } catch (e) {
//                 console.log(e);
//                 reject(false);
//             }
//         });
//     }

    memoList: Memo[] = [];
    
    // getMemoList(): Promise<Memo[]> {
    //     return new Promise(resolve => {
    //         for(let i = 0; i < localStorage.length; i++){
    //             let id = localStorage.getKey(i);
    //             let contents = localStorage.getItem(id).toString();
    //             this.memoList.push({id, contents});
    //         }

    //         return Promise.resolve(this.memoList);
    //     });
    // }

    getMemoListMock(): Memo[] {
        console.log("getMockList");
        for(let i = 0; i < 10; i++){
            let id = i;
            let contents = "localStorage.getItem(id).toString() - " + i;
            this.memoList.push({id, contents});
        }

        return this.memoList;
    }

    getMemo(id: number): Memo {
        return this.memoList.find(memo => memo.id === id);;
    }

    writeMemo(memo: Memo): Promise<Memo> {
        return new Promise(resolve => {
            let memoId = localStorage.length.toString();
            localStorage.setItem(memoId, memo.contents);
        });
    }
}