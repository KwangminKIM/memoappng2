import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';

import { AppComponent } from './app.component';

import { MemoComponent } from './memo/memo.component';
import { MemoDetailComponent } from './memo/detail/memo-detail.component';
import { MemoModifyComponent } from './memo/modify/memo-modify.component';
import { MemoWriteComponent } from './memo/write/memo-write.component';
import { MemoService } from './_services/memo.service';

@NgModule({
    imports : [
        BrowserModule,
        FormsModule,
    ],
    declarations : [
        AppComponent,
        MemoComponent,
        MemoDetailComponent,
        MemoModifyComponent,
        MemoWriteComponent
    ],
    providers: [
        MemoService
    ],
    bootstrap : [
        AppComponent
    ]
})
export class AppModule {}
