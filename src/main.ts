
import 'core-js';
import 'reflect-metadata';
import 'zone.js';
import 'rxjs';

import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { AppModule } from './app.module';

platformBrowserDynamic().bootstrapModule( AppModule );
