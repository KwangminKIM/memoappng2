import { Component } from '@angular/core';

import { Memo } from '../../_classes/memo';
import { MemoService } from '../../_services/memo.service';

@Component({
    selector: 'memo-write',
    templateUrl: './memo-write.component.html',
    styleUrls: ['../memo.component.css']
})
export class MemoWriteComponent {

    memo: Memo;

    constructor(
        private memoService: MemoService,
    ){}
    
    write(): void {
        // if(this.memo.contents = ""){
        //     alert("본문을 입력해주세요.");
        //     return;
        // }

        this.memoService.writeMemo(this.memo).then(() => {
            
        });
    }

    goBack(): void {
        
    }

}