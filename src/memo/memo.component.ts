import { Component, OnInit } from '@angular/core';

import { Memo } from './../_classes/memo';
import { MemoService } from '../_services/memo.service';

@Component({
    selector: 'memo-app',
    templateUrl: './memo.component.html',
    styleUrls: ['./memo.component.css']
})
export class MemoComponent implements OnInit{

    // 삭제 버튼 클릭 시 수행 - 서비스의 메소드를 호출한 결과 돌려받는 객체를 화면에 보여주도록 한다.
    // $scope.deleteMemo = function(memoInfo){
    //     if(confirm('삭제 시 복구할 수 없습니다. 계속하시겠습니까?')) {
    //         storage.deleteMemo( memoInfo.memoId ).then(function(data){
    //             //삭제 완료된 경우
    //             if(data) {
    //                 storage.getMemoList().then(function(data){
    //                     $scope.contents = data;
    //                 });
    //             } else {
    //             //삭제되지 않은 경우
    //             }
    //         });
    //     }
    // };
    flag: string;
    memoList: Memo[] = [];
    curSelMemo: Memo;

    constructor(private memoService: MemoService){}

    ngOnInit(): void {
        console.log("init");
        this.memoList = this.memoService.getMemoListMock();
        this.flag = "list";
    }

    viewDetail(memo){
        this.flagChange("detail");
        console.log(memo);
        this.curSelMemo = memo;
    }

    flagChange(flag){
        this.flag = flag;
        console.log(flag);
    }
}